import csv
import random

def groupe(data):
    with open(data, 'r') as file:
        reader = csv.reader(file, delimiter=';')
        next(reader)
        data = [row for row in reader]

    random.shuffle(data)

    with open('binomes.csv', 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(['Groupe', 'Prenom1', 'Nom1', 'Prenom2', 'Nom2'])
        for i in range(0, len(data), 2):
            if i + 1 < len(data):
                writer.writerow([(i // 2) + 1, data[i][1], data[i][0], data[i + 1][1], data[i + 1][0]])
            else:
                writer.writerow([(i // 2) + 1, data[i][1], data[i][0], '', ''])
            
        print("Les groupes ont été crées")

def main():
    if __name__ == "__main__":
        main()